       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DoCalc.
       AUTHOR. Thanawat Thongtitcharoen.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  FirstNum    PIC 9       VALUE ZEROS.
       01  SecondNum   PIC 9       VALUE ZEROS.
       01  CalcResult  PIC 9       VALUE 0.
       01  UserPromot  PIC X(38)   VALUE
                       "Please enter two single digit numbers".
       PROCEDURE DIVISION.
       CalculateResult.
           DISPLAY UserPromot 
           ACCEPT FirstNum 
           ACCEPT SecondNum 
           COMPUTE CalcResult = FirstNum + SecondNum 
           DISPLAY "Result is = ", CalcResult 
           STOP RUN.